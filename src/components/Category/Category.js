import React from 'react';
import { Link } from 'react-router-dom';
 
// Category component
const Category = ({ match }) => {
  console.log(match);
  
  return (
    <div>
      <ul>
        <li><Link to={`${match.url}/shoes`}>Shoes</Link></li>
        <li><Link to={`${match.url}/boots`}>Boots</Link></li>
        <li><Link to={`${match.url}/flats`}>Flats</Link></li>      
      </ul>

      
    </div>
  )
}

export default Category;
