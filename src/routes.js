import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/Home/Home';
import Category from './components/Category/Category';
import Products from './components/Products/Products';
import Product from './components/Product/Product';


const GetRoutes = ({ match }) => (

  <Switch>
    <Route exact path="/" component={Home}/>
    <Route path="/category" component={Category}/>
    <Route path="/products" component={Products}/>

    {/* category routes */}
    <Route path={`${match.url}/:name`} render={({match}) => (
      <div>
        <h3>{match.params.name}</h3>
      </div>
    )}/>

    {/* products component */}
    <Route path={`${match.url}/:productId`}
      render={ (props) => <Product data= {productData} {...props}/>}
    />
    <Route exact path={match.url}
      render={() => (
        <div>Please select a product.</div>
      )}
    />
  </Switch>
)

export default GetRoutes;