import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import GetRoutes from './routes';

class App extends Component {
  render() {
    return (
      <div className="App">
        <nav className="nav">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/category">Category</Link>
            </li>
            <li>
              <Link to="/products">Products</Link>
            </li>
          </ul>
        </nav>

        <GetRoutes/>
      </div>
    );
  }
}

export default App;
